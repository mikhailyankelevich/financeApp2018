//
//  main.cpp
//  financeApp
//
//  Created by Mikhail Yankelevich on 13/05/2018.
//  Copyright © 2018 Mikhail Yankelevich. All rights reserved.
//

#include "HeaderFinanceApp.h"


int main() {
    FILE *data =fopen(mainDatabaseAddress, "r+");
    if (!data)
    {
        cout<<"no file found"<<endl;
        return 0;
    }
    fclose(data);
    menu();
    return 0;
}
