//
//  menu.cpp
//  financeApp
//
//  Created by Mikhail Yankelevich on 13/05/2018.
//  Copyright © 2018 Mikhail Yankelevich. All rights reserved.
//

#include "HeaderFinanceApp.h"
void printMenuVariants(){
    cout<<"To add a new spending enter 1"<<endl;
    cout<<"To open print menu enter 2"<<endl;
    cout<<"To open finance analyse menu enter 3"<<endl;
    cout<<"To quit program enter 0"<<endl;
    cout<<"\n\n\n"<<endl;

}

void printPrintMenuVariants(){
    cout<<"To print all data enter  1"<<endl;
    cout<<"To print out last month enter 2"<<endl;
    cout<<"To print out last week enter 3"<<endl;
    cout<<"To go back to the main menu enter 0"<<endl;
    cout<<"\n\n\n"<<endl;
}

void printFinanceAnalyseMenuVariants(){
    cout<<"To print shared spendings summ for last month enter 1"<<endl;
    cout<<"To print personal spendings summ for last month enter 2"<<endl;
    cout<<"To go back to the main menu enter 0"<<endl;
    cout<<"\n\n\n"<<endl;
}

void printMenu(){
    printPrintMenuVariants();
    int choice=getIntFromUser();
    while (choice!=0)
    {
        if (choice==1)
            printAllData();
        else if (choice==2)
            printOutLastMonth();
        else if (choice==3)
            lastWeekSpendings();
        printPrintMenuVariants();
        choice=getIntFromUser();
    }

}




void analyseMenu(){
    printFinanceAnalyseMenuVariants();
    int choice=getIntFromUser();
    while (choice!=0)
    {
        if (choice==1)
            printSharedSummLastMonth();
        else if (choice==2)
            printPersonalSummLastMonth();
        printFinanceAnalyseMenuVariants();
        choice=getIntFromUser();
    }
}



void developerMenu(){
    cout<<"!!!!!Developer menu!!!!"<<endl;

}

void menu()
{

    printMenuVariants();
    int choice=getIntFromUser();
    int IdMax;
    getFinallId(&IdMax);
    while (choice!=0)
    {
        if (choice==1)
            recordNewSpending(&IdMax);
        if (choice==2)
            printMenu();
        if (choice==3)
            analyseMenu();
        if (choice==100)
            developerMenu();
        // if (choice==4)
        //     editUnit();

        printMenuVariants();
        choice=getIntFromUser();
    }
}
