//
//  rintingData.cpp
//  financeApp
//
//  Created by Mikhail Yankelevich on 13/05/2018.
//  Copyright © 2018 Mikhail Yankelevich. All rights reserved.
//

#include "HeaderFinanceApp.h"

bool developer=false;


void  printOutLastMonth()
{
 
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r+");
    int dayCur=sp.day;
    int monthCur=sp.month;
    int monthPrev;
    if (sp.month==1)
        monthPrev=12;
    else monthPrev=monthCur-1;
    fseek(data, -91, SEEK_END);
    fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    
    while(((sp.day<=dayCur&&sp.month==monthCur)|| (sp.day>=dayCur&&sp.month==monthPrev))&&(sp.Id>1))
    {
        // printScannedInfo(&sp);
        fseek(data, (-91)*2, SEEK_CUR);
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    }



    fseek(data, (-91), SEEK_CUR);
     while(!feof(data))
    {
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        printScannedInfo(&sp);

    }

    fclose(data);
}





void printAllData()
{
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r");
    
    while(!feof(data))
    {
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        printScannedInfo(&sp);

    }
//    cout<<"week day is"<<sp.weekDay<<endl;
    fclose(data);
}




void printScannedInfo(class Spending *sp)
{
    cout<<"\n\n"<<endl;
    if (developer)
    cout<<sp->Id<<endl;
    cout<<"Spending on: "<<sp->day<<"."<<sp->month<<"."<<sp->year<<endl;
    cout<<"Sex:"<<sp->sex<<endl;
    cout<<"Food:"<<sp->food<<endl;
    cout<<"Crap:"<<sp->crap<<endl;
    cout<<"Dates:"<<sp->dates<<endl;
    cout<<"\nAs personal spendings total:"<<sp->getSummPersonal()<<endl;
    cout<<"As shared spendings total:"<<sp->getSummShared()<<endl;
    cout<<"\n\n"<<endl;

}




void  printOutLastYear()
{
    
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r+");
//    int dayCur=sp.day;
    int yearCur=sp.month;
//    int yearPrev=yearCur-1;
//    if (sp.month==1)
//        monthPrev=12;
//    int monthCur=sp.month;
//    else monthPrev=monthCur-1;
    fseek(data, -91, SEEK_END);
    fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    
    while(sp.year==yearCur&&(sp.Id>1))
    {
        // printScannedInfo(&sp);
        fseek(data, (-91)*2, SEEK_CUR);
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    }
    
    fseek(data,-91,SEEK_CUR);
     while(!feof(data))
    {
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        printScannedInfo(&sp);

    }

    fclose(data);
}


void lastWeekSpendings(){
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r+");
    
    fseek(data, -91, SEEK_END);
    fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    int prevMonth=sp.curMonth-1;
    if (prevMonth==0)
        prevMonth=12;
    int prevDay=sp.curMonth-7;
    if (prevDay<0)
    {
        if (prevMonth==2)
        {
            prevDay=28-prevDay;
        }
        else if (prevMonth%2==1)
        {
            prevDay=31-prevDay;
        }
        else if (prevMonth%2==0)
        {
            prevDay=30-prevDay;
        }
        
        
        while(!feof(data)&&sp.Id>1&&(sp.month==sp.curMonth||(sp.month==prevMonth&&prevDay<sp.day)))
        {
            // printScannedInfo(&sp);
            fseek(data, (-91)*2, SEEK_CUR);
            fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        }
    }
    else
    {
        while ((!feof(data))&&sp.Id>1&&(sp.month==sp.curMonth&&sp.day>=prevDay))
        {
            // printScannedInfo(&sp);
            fseek(data, (-91)*2, SEEK_CUR);
            fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        }
    }

     while(!feof(data))
    {
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
        printScannedInfo(&sp);

    }

    fclose(data);
}
