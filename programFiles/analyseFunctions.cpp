//
//  analyseFunctions.cpp
//  financeApp
//
//  Created by Mikhail Yankelevich on 14/05/2018.
//  Copyright © 2018 Mikhail Yankelevich. All rights reserved.
//

#include "HeaderFinanceApp.h"

void printPersonalSummLastMonth()
{
    int summ=0;
    
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r+");
    int dayCur=sp.day;
    int monthCur=sp.month;
    int monthPrev;
    if (sp.month==1)
        monthPrev=12;
    else monthPrev=monthCur-1;
    fseek(data, -91, SEEK_END);
    fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    
    while(((sp.day<=dayCur&&sp.month==monthCur)|| (sp.day>=dayCur&&sp.month==monthPrev))&&(sp.Id>1))
    {
        summ+=sp.getSummPersonal();
        fseek(data, (-91)*2, SEEK_CUR);
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    }
    cout<<"\npersonal summ last month: "<<summ<<endl;
    fclose(data);
}



void printSharedSummLastMonth()
{
    int summ=0;
    
    Spending sp;
    FILE *data=fopen(mainDatabaseAddress, "r+");
    int dayCur=sp.day;
    int monthCur=sp.month;
    int monthPrev;
    if (sp.month==1)
        monthPrev=12;
    else monthPrev=monthCur-1;
    fseek(data, -91, SEEK_END);
    fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    
    while(((sp.day<=dayCur&&sp.month==monthCur)|| (sp.day>=dayCur&&sp.month==monthPrev))&&(sp.Id>1))
    {
        summ+=sp.getSummShared();
        fseek(data, (-91)*2, SEEK_CUR);
        fscanf(data,"%d%d%d%d%d%d%d%d",&sp.Id,&sp.sex,&sp.food, &sp.crap, &sp.dates, &sp.day, &sp.month, &sp.year);
    }
    cout<<"\nShared summ last month: "<<summ<<endl;
    fclose(data);
}
