//
//  Header.h
//  financeApp
//
//  Created by Mikhail Yankelevich on 13/05/2018.
//  Copyright © 2018 Mikhail Yankelevich. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <string>
#define mainDatabaseAddress "/Users/macbook/Desktop/financeApp/financeApp/financeApp/mainDatabase.txt"
using namespace std;


class CourentDate{
public:
    CourentDate(){
        time_t now = time(0);
        tm *ltm = localtime(&now);
        year=1900 + ltm->tm_year;
        month=1 + ltm->tm_mon;
        day= ltm->tm_mday;
        curWeekDay=ltm->tm_wday;
        curDay= ltm->tm_mday;
        curMonth=1 + ltm->tm_mon;
        curYear=1900 + ltm->tm_year;
    }
    int day;
    int year;
    int month;
    int curWeekDay;
    int curDay;
    int curMonth;
    int curYear;

};

class Spending:public CourentDate{
public:
    int Id;
    int sex;
    int food;
    int crap;
    int dates;
    int others;
    int getSummPersonal(){
        return sex+food+crap+dates;
    }
    int getSummShared(){
        return dates+sex+food;
    }

};

extern bool developer;

void menu();
int getIntFromUser(int enableEnterEqualTo0=0);
void getFinallId(int *IdMax);
void recordNewSpending(int *IdMax);
void printOutLastMonth();
void printAllData();
void printScannedInfo(class Spending *sp);
void lastWeekSpendings();
void printSharedSummLastMonth();
void printPersonalSummLastMonth();
void editUnit();
